## La primera programadora
<p style="text-align: justify;">Les notes de Lovelace estan classificades alfabèticament de la A a la G. En la nota G, descriu un algorisme per programar la màquina analítica de Babbage amb nombres de Bernoulli, mitjançant un programa de targetes perforades. Es considera el primer algorisme específicament creat per programar un ordinador (en aquell temps, una màquina computadora). Per aquesta raó Ada Lovelace es considera la primera programadora informàtica. De tota manera, com que la màquina mai fou completada, la seva programació no es va provar.

Per al disseny de l'algoritme comentat anteriorment, es basà en els telers de Jacquard. Aquests telers comptaven amb un sistema de targetes perforades que els permetien crear diferents tipus de dibuixos sobre les teles de manera mecànica. Ada Lovelace partí d'aquesta idea per a inventar un sistema de targetes perforades capaç de calcular de manera automàtica els Nombres de Bernoulli.

El 1953, més d'un segle després de la seva mort, les notes d'Ada Lovelace sobre la màquina analítica de Babbage es van tornar a publicar. Avui en dia, l'esmentada màquina analítica es reconeix com el primer model d'ordinador, i les notes de Lovelace com la primera descripció d'un programari.</p>

<img src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Diagram_for_the_computation_of_Bernoulli_numbers.jpg/1280px-Diagram_for_the_computation_of_Bernoulli_numbers.jpg" width="600" height="500">

Ada Lovelace aclareix que la màquina analítica **no era una forma d'intel·ligència artificial**. 

_"La màquina analítica no té pretensions d'originar res. Pot fer allò que nosaltres li ordenem que faci. Pot fer el seguiment d'una anàlisi; però no té cap poder per anticipar cap relació analítica o cap veritat"_

**Afirmava que la màquina seria capaç de fer qualsevol cosa que li demanéssim, però que era necessari que nosaltres sabéssim com donar-li instruccions**. 

_"Les màquines analítiques podran fer qualsevol cosa, sempre que sapiguem com ordenar-li que ho facin"_

<img src="https://s.libertaddigital.com/fotos/noticias/1920/1080/fit/maquina-analitica-babbage.jpg" width="600" height="500">

[Draw my life de Ada Lovelace](Youtube.md)
