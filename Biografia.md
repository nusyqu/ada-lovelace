# Ada Lovelace

<img src="https://historia.nationalgeographic.com.es/medio/2020/11/25/ada-lovelace-portrait_b0909676_550x789.jpg" width="300" height="">

## Biografía
<p style="text-align: justify;">Augusta Ada Byron King, comtessa de Lovelace. Hija del famoso poeta Lord Byron, nació el 10 de diciembre de 1815, en Londres y desafió las limitaciones que imponía la lógica victoriana para la mujer. Su pasión y esmero dio resultados: creó lo que hoy se conoce como el primer algoritmo pensado para ser procesado por una máquina. De ahí que se la considera la primera programadora de computadoras.

Ada escribió el algoritmo para calcular los valores de los números de Bernoulli utilizando dos bucles, detalló como hacer operaciones trigonométricas que empleaban variables en la máquina analítica de Babbage y definió el uso de tarjetas perforadas para programar la máquina.</p>

[Trabajo](Algoritmo.md)
